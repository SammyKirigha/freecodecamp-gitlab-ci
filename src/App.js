import React from "react";
import logo from "./gitlab.svg";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <a
          className="App-link"
          href="https://gitlab.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn GitLab CI today
        </a>
        <h1>HI Sammy</h1>
        <h1>Nothing is so amazing than understanding devops</h1>
      </header>
    </div>
  );
}

export default App;
